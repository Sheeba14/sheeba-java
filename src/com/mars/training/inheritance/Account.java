package com.mars.training.inheritance;

public class Account {
	
	String accountType;
	int accountNbr;
	double balance;
	
	public void deposit(double amount) {
		balance = balance + amount;
	}

	
	public void withdraw(double amount) {
		balance = balance - amount;
	}
	
	public String getDetail() {
		return "Account Type " + accountType + " for the Customer account number " + accountNbr + " has balance " + balance;
	}
}
