package com.mars.training.inheritance;

import java.util.Scanner;

public class AccountTest {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("what type of account you want to open?");

		String type  = sc.next();

		Account account = null;
	
	
		if (type.equalsIgnoreCase("Savings")) {
			account = new SavingsAccount();
			
		} else if (type.equalsIgnoreCase("Checking")) {
			account = new CheckingAccount();
			
		} else {
			System.out.println ("Please enter either Savings or Checking");
		}

		account.accountType = type;
		
		System.out.print("Enter Account Number: ");
		int num = sc.nextInt();
		
		account.accountNbr = num;
		
		System.out.print("Enter Initial Balance: ");
		
		double bal = sc.nextDouble();
		account.balance = bal;		
		
		System.out.print("1: Deposit, 2: withdrawl");
		
		int choice = sc.nextInt();

		System.out.print("Enter amount: ");
		
		double amount = sc.nextInt();
		
		if (choice == 1) {
			account.deposit(amount);
		} else {
			if (choice == 2) {
				account.withdraw(amount);
			} else {
				System.out.println("Invalid choice.");
			}
		}
		
		System.out.println(account.getDetail());
		
		
		

			
	}

}
