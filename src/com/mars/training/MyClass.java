package com.mars.training;

import java.util.Scanner;

public class MyClass {
	
	public static void main(String args[]) {
		System.out.println("Hello World");
		
		Employee emp1 = new Employee();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter Employee Name: ");
		String empName = sc.next();
		System.out.println();
		
		System.out.print("Enter Employee Designation: ");
		String empDesignation = sc.next();
		System.out.println();		

		System.out.print("Enter Employee Hours Worked: ");
		int hoursWorked = sc.nextInt();
		System.out.println();
	
		emp1.setEmpName(empName);
		emp1.setEmpDesignation(empDesignation);
		emp1.setHoursWorked(empDesignation);
		
		System.out.println("Object creation complete");
		
	}

}
